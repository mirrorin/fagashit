title: Spring 2014 (Gokukoku no Brynhildr)
date: 2014-04-05 11:58:43
tags:
categories: Docs
description: 
feature: 
---

<p align="justify">A high school senior Murakami Ryouta had a childhood friend called Kuroneko who died in an accident. Ryouta had made a promise with Kuroneko that he will prove there are aliens in the universe. In order to fulfill his promise, he joined the astronomy club at the school as its only member and kept observing the stars almost every night. One day, a girl who looks like Kuroneko transferred into his class. To his surprise, the girl's name is Kuroha Neko which is really similar to his childhood friend's name. However it seems that there is something unusual about her. It turns out that she is a witch who actually escaped from a research laboratory. </p>

Source: ytv

<!-- more -->

![](http://i.imgur.com/hKLFhau.jpg "Gokukoku no Brynhildr (Brynhildr in the Darkness)")

Some Staff : <br>

* Translation : TBA
* TL Checking : TBA
* Editing : TBA
* Typesetting : TBA
* Timing : TBA
* Encoding: TBA
* Quality Checking : TBA
* Song Translation : TBA
* Song Styling : TBA